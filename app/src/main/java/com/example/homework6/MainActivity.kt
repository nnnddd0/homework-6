package com.example.homework6

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE)

    }

    fun save(view:View){
        val firstname = firstnametext.text.toString()
        val lastname = lastnametext.text.toString()
        val address = addresstext.text.toString()
        val email = emailtext.text.toString()
        val age = agetext.text.toString().toInt()

        if (firstname.isNotEmpty() && lastname.isNotEmpty() && address.isNotEmpty() && email.isNotEmpty() && age.toString().isEmpty()) {
            val editor = sharedPreferences.edit()
            editor.putString("firstname", firstname)
            editor.putString("lastname", lastname)
            editor.putString("email", email)
            editor.putInt("age", age)
            editor.putString("address", address)
            editor.apply()
            Toast.makeText(this, "Information Saved Successfully", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "All fields must be filled!", Toast.LENGTH_SHORT).show()
        }
    }

    fun read(view: View){
        val firstnamesaved = sharedPreferences.getString("firstname", "")
        val lastnamesaved = sharedPreferences.getString("lastname", "")
        val emailsaved = sharedPreferences.getString("email", "")
        val agesaved = sharedPreferences.getInt("age", 0)
        val addresssaved = sharedPreferences.getString("address", "")

        firstnametext.setText(firstnamesaved)
        lastnametext.setText(lastnamesaved)
        emailtext.setText(emailsaved)
        agetext.setText(agesaved.toString())
        addresstext.setText(addresssaved)
    }
}